package org.example;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String path = "./src/test/java/page/child";
        File dir = new File(path);
        File[] arrFiles = dir.listFiles();

        for(int i = 0; i < arrFiles.length; i++) {
            System.out.println(arrFiles[i].getName().replace("java", "class"));
        }
    }
}