package page;

import page.child.*;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.page;

public class ListClass {
    protected Map<String, Page> classPage = new HashMap<>();

    {
        classPage.put("Dynamic ID", page(DynamicIdPage.class));
        classPage.put("Class Attribute", page(ClassAttributePage.class));
        classPage.put("Hidden Layers", page(HiddenLayersPage.class));
        classPage.put("AJAX Data", page(AjaxDataPage.class));
        classPage.put("Client Side Delay", page(ClientSideDelayPage.class));
        classPage.put("Click", page(ClickPage.class));
        classPage.put("Text Input", page(TextInputPage.class));
        classPage.put("Scrollbars", page(ScrollbarsPage.class));
        classPage.put("Dynamic Table", page(DynamicTablePage.class));
        classPage.put("Verify Text", page(VerifyTextPage.class));
        classPage.put("Progress Bar", page(ProgressBarPage.class));
        classPage.put("Visibility", page(VisibilityPage.class));
        classPage.put("Sample App", page(SampleAppPage.class));
        classPage.put("Mouse Over", page(MouseOverPage.class));
        classPage.put("Non-Breaking Space", page(NonBreakingSpacePage.class));
        classPage.put("Overlapped Element", page(OverlappedElement.class));
        classPage.put("Shadow DOM", page(ShadowDomPage.class));
        classPage.put("Load Delay", page(LoadDelayPage.class));
    };
}
