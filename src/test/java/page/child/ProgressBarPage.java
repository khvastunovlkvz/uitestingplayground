package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class ProgressBarPage extends Page {

    private static final SelenideElement startButton = $(By.id("startButton"));
    private static final SelenideElement stopButton = $(By.id("stopButton"));
    private static final SelenideElement progressBar = $(By.id("progressBar"));

    public static SelenideElement getStartButton() {
        return startButton;
    }

    public static SelenideElement getStopButton() {
        return stopButton;
    }

    public static SelenideElement getProgressBar() {
        return progressBar;
    }

    @Override
    public Page waiLoad(String load) {
        while (!progressBar.getText().equals(load));
        return this;
    }



}
