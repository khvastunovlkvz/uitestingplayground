package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class TextInputPage extends Page {

    private static final SelenideElement input = $(By.id("newButtonName"));
    private static final SelenideElement button = $(By.id("updatingButton"));

    public static SelenideElement getButton() {
        return button;
    }

    public static SelenideElement getInput() {
        return input;
    }
}
