package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class ClientSideDelayPage extends Page {

    private static final SelenideElement button = $(By.id("ajaxButton"));
    private static final SelenideElement label = $x("//p[@class=\"bg-success\"]");

    public static SelenideElement getButton() {
        return button;
    }

    public static SelenideElement getLabel() {
        return label;
    }
}
