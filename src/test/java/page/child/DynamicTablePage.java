package page.child;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import page.Page;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class DynamicTablePage extends Page {

    private static final ElementsCollection dateChrome = $$x("//span[text()=\"Chrome\"]/../span");
    private static final ElementsCollection collectionName = $$x("//span[text()=\"Name\"]/../span");

    private static final SelenideElement valueCpuChrome = $(".bg-warning");

    /** Метод возвращает индекс элемента по названию */
    private int getIndexCollection(String name) {
        int index = 0;
        for(int i = 0; i < collectionName.size(); i++) {
            if (collectionName.get(i).getText().equals(name)) {
                index = i;
                break;
            }
            else {
                index = 0;
            }
        }
        return index;
    }

    private String textProcent(String text) {
        String[] tempArray = text.split(" ");
        return tempArray[tempArray.length - 1];
    }

    @Override
    public String searchDinamycTable(String name) {
        return dateChrome.get(getIndexCollection(name)).getText();
    }

    @Override
    public String getText(SelenideElement element) {
        return  textProcent(element.getText());
    }

    public static SelenideElement getValueCpuChrome() {
        return valueCpuChrome;
    }
}
