package page.child;

import com.codeborne.selenide.SelenideElement;
import page.Page;

import static com.codeborne.selenide.Selenide.$x;

public class NonBreakingSpacePage extends Page {

    private static final SelenideElement button = $x("//button[text()=\"My Button\"]");

    public static SelenideElement getButton() {
        return button;
    }
}
