package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class OverlappedElement extends Page {

    private static final SelenideElement id = $(By.id("id"));
    private static final SelenideElement name = $(By.id("name"));

    public static SelenideElement getId() {
        return id;
    }

    public static SelenideElement getName() {
        return name;
    }
}
