package page.child;

import com.codeborne.selenide.SelenideElement;
import page.Page;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MouseOverPage extends Page {

    private static final SelenideElement hrefText = $x("//a[contains(text(), \"Click me\")]");
    private static final SelenideElement textCount = $("#clickCount");

    public static SelenideElement getHrefText() {
        return hrefText;
    }

    public static SelenideElement getTextCount() {
        return textCount;
    }
}
