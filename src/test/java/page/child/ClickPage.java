package page.child;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import page.Page;

import static com.codeborne.selenide.Selenide.$;

public class ClickPage extends Page {

    private static final SelenideElement button = $(By.id("badButton"));


    public static SelenideElement getButton() {
        return button;
    }
}
