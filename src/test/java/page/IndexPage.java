package page;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import page.child.*;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.*;

public class IndexPage extends ListClass{

    private final ElementsCollection formSelectTest = $$x("//h3/a");
    private Map<String, SelenideElement> mapHref = new HashMap<>();

    public IndexPage(String url) {
        open(url);
        addInHashMapHref();
    }

    /** Метод для перехода на нужную страницу для теста */
    public Page nameTest(String name) {
        mapHref.get(name).click();
        return classPage.get(name);
    }
    /** Метод для добавления ссылок в HashMap */
    private void addInHashMapHref() {
        for (int i = 0; i < formSelectTest.size(); i++) {
            mapHref.put(formSelectTest.get(i).getText(), formSelectTest.get(i));
        }
    }

}
