package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.sleep;

public abstract class Page {

    /** Нажатие кнопки */
    public Page clickBtn(SelenideElement button) {
        button.click();
        return this;
    }

    /** Клик по всплывающему окну */
    public Page clickAlert() {
        return this;
    }

    /** Метод для ожидания появления элемента строки */
    public Page checkLabel(SelenideElement element, String name, int timeSleep) {
        sleep(timeSleep * 1000); // timeSleep указывается в секундах
        element.shouldHave(Condition.text(name));
        return this;
    }

    /** Метод для получения аттрибута элемента */
    public String getAttribute(SelenideElement element, String attributte) {
        return element.getAttribute(attributte);
    }

    /** Метод для ввода текста в поле */
    public Page enterText(SelenideElement input, String text) {
        input.setValue(text);
        return this;
    }

    /** Метод для получения названия элемента */
    public String getText(SelenideElement element) {
        return  element.getText();
    }

    /** Метод для прокрутки до необходимого элемента */
    public Page scroll(SelenideElement element) {
        element.scrollIntoView(true);
        return this;
    }

    /** Получение данных из динамической таблицы */
    public String searchDinamycTable(String name) {
        return "";
    }

    /** Ожидание загрузки до указанного значения load */
    public Page waiLoad(String load) {
        return this;
    }

    /** Метод для проверки, что элемент видим, если visible = true и невидим, если visible = false */
    public Page visible(SelenideElement element, boolean visible) {
        if (visible) element.should(Condition.visible);
        else element.shouldNot(Condition.visible);
        return this;
    }

    /** Метод для получения значения элемента */
    public String getVal(SelenideElement element) {
       return element.getValue();
    }

}
