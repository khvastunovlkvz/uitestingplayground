import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {

    protected static final String BASE_URL = "http://uitestingplayground.com/";
    private void setup() {
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
    }

    @BeforeSuite
    public void init() {
        setup();
    }

    @AfterSuite
    public void tearDown() {
        Selenide.closeWebDriver();
    }

}
